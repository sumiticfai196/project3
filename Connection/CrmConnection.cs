﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Crm.Sdk;
using System.Runtime.Serialization;
using Microsoft.Xrm.Sdk.Client;
using System.ServiceModel;
using Microsoft.Crm.Sdk.Messages;
using System.ServiceModel.Description;


namespace ConsoleApp_CrmConnection.Connection
{
    public class CrmConnection
    {

        ClientCredentials credentials = new ClientCredentials();
        credentials.UserName.UserName = UserName;
        credentials.UserName.Password = Password;
        Uri serviceUri = new Uri(SoapOrgServiceUri);
        OrganizationServiceProxy proxy = new OrganizationServiceProxy(serviceUri, null, credentials, null);
        proxy.EnableProxyTypes();
        _service = (IOrganizationService) proxy;


    }
}

